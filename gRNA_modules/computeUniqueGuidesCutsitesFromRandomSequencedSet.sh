mkdir -p SampledFromReference
sample() {
	toSample=$1
	FILE="GRCh38.p10.guides.joined.sortedbyabundance_HGG.bed.gz"
#	FILE="temp"
	zcat $FILE | shuf -n $toSample | awk '{print $7, $8}' | awk '!a[$0]++' | awk -v s=$toSample '{uniqueguides++; cutsites+=$2;}END{print s, uniqueguides, cutsites}' > SampledFromReference/${toSample}_hgg.out
}
export -f sample
parallel --record-env
parallel -j5 sample ::: $(cat <(seq 100000 100000 900000) <(seq 1000000 500000 5000000) <(seq 6000000 1000000 10000000) <(seq 12500000 2500000 25000000) <(seq 30000000 5000000 100000000) <(seq 110000000 10000000 200000000))
