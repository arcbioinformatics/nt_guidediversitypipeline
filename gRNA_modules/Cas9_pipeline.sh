
if [ "$#" -ne 3 ]; then
	printf "\n Need files_paired, DNA/RNA, and design file as argument.\n"
	printf "\n Make sure files_paired has fastq.gz paired end read files, R1 first, followed by R2 separated by a space\n"
	exit 1;
fi

NA=$2
if [[ $NA == "DNA" || $NA == "RNA" ]]; then
	echo "Pipeline running with $NA input"
else
	echo "Pipeline needs DNA or RNA as input"
	exit 1;
fi

if [[ $NT_GD_SCRIPT_DIR == "" ]]; then
	echo "Set NT_GD_SCRIPT_DIR env var before you run the pipeline"
	exit 1;
else
	echo "NT_GD_SCRIPT_DIR set to $(echo $NT_GD_SCRIPT_DIR)"
fi

FILES_PAIRED=$1
DESIGN=$3

absent_array_count=$(cat $DESIGN  | awk '{print $3}' | sort | uniq | fgrep -ivwf ${NT_GD_SCRIPT_DIR}/gRNA_modules/arrays -  | wc -l)
if [ $absent_array_count != "0" ]; then
	echo "Primer pool splitup will not be drawn up for one or more of the guide arrays. Update ${NT_GD_SCRIPT_DIR}/gRNA_modules/arrays and try again"
	exit 1;
fi

processfastq() {
	R1=$1
	R2=$2
	PROMOTER_SEQ=$3
	STEMLOOP_SEQ=$4
	OUT_FOLDER=$5
	NA=$6
	RG=`basename $R1 _R1_001.fastq.gz`
	
	#AdapterRemoval, keep only collapsed reads. These are high confidence reads we have evidence
	#by sequencing from both ends
	
	if [ ! -f ${OUT_FOLDER}/${RG}/${RG}.collapsed.gz ]; then 
		echo "Merging read 1 and read 2 for library $RG, keeping collapsed fastq, discarding other files"
		/mnt/bdbStorage01/programs/adapterremoval/build/AdapterRemoval --threads 20 --file1 $R1 --file2 $R2 --gzip --basename $RG --trimns --trimqualities --collapse
		mkdir -p ${OUT_FOLDER}/${RG}
		mv ${RG}.* ${OUT_FOLDER}/${RG}/
		rm -f ${OUT_FOLDER}/${RG}/*.singleton.truncated.gz ${OUT_FOLDER}/${RG}/*.discarded.gz ${OUT_FOLDER}/${RG}/*.pair1.truncated.gz ${OUT_FOLDER}/${RG}/*.pair2.truncated.gz ${OUT_FOLDER}/${RG}/*.collapsed.truncated.gz
	else
		echo "Collapsed fastq exists, not rerunning AdapterRemoval"
	fi

	if [ ! -f ${OUT_FOLDER}/${RG}/${RG}.18mers_18-22.txt.gz ]; then 	
		if [ $NA == "DNA" ]; then
			#Tease out the middle sequence from the collapsed fastq, look for T7 and SL sequences
			echo "Teasing out guide sequence for $NA from library $RG ..."
			zcat ${OUT_FOLDER}/${RG}/${RG}.collapsed.gz | grep -o "${PROMOTER_SEQ}.*${STEMLOOP_SEQ}" | sed -e "s/${PROMOTER_SEQ}//" -e "s/${STEMLOOP_SEQ}$//" | awk '{arr[$1]++;}END{for (i in arr){print i, arr[i]}}' | gzip > ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz
			zcat ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz | awk 'NF==1' | awk '{print "0", $1}' > ${OUT_FOLDER}/${RG}/${RG}.seqsummary.txt
			zcat ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz | awk 'NF==2' | awk '{print length($1), $2}' | awk '{arr[$1]+=$2}END{for(i in arr){print i, arr[i]}}' | sort -n >> ${OUT_FOLDER}/${RG}/${RG}.seqsummary.txt
		#	zcat ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz | cut -f 1 -d " " | awk 'length($1)>=18 && length($1)<=22' | rev | cut -c 1-18 | rev | awk 'BEGIN{sum=0;}{arr[$1]++;sum++;}END{for(i in arr){print i, arr[i], sum, arr[i]/sum}}' | sort -k 2,2nr > ${OUT_FOLDER}/${RG}/${RG}.18mers_18-22.txt | this is wrong - not accounting for number of times an Nx is already present
			echo "Generating 18-mers from 18-22mer fraction in library $RG ..."	
			zcat ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz | awk 'NF==2' | awk 'length($1)>=18 && length($1)<=22' | awk '{print substr($1, length($1)-18+1), $2}' | awk 'BEGIN{sum=0;}{arr[$1]+=$2; sum+=$2;}END{for(i in arr){print i, arr[i], sum, arr[i]/sum}}' | sort -k 2,2nr > ${OUT_FOLDER}/${RG}/${RG}.18mers_18-22.txt 
		else
			#RNA pipeline - output counts after adjusting for UMIs	
			echo "Teasing out guide sequence for $NA from library $RG ..."
			zcat ${OUT_FOLDER}/${RG}/${RG}.collapsed.gz  |  sed -n '2~4p' | perl -ne 'm/GGG.{0,3}?G[AG]G(\w{0,140})GTTTT.*GGCTA(\w{5,8})/ && print "$1\t$2\n"' |  awk '{arr[$0]++;}END{for(i in arr){print i, arr[i]}}' | gzip > ${OUT_FOLDER}/${RG}/${RG}.fixedcounts.gz
			noinsert_count=$(zcat ${OUT_FOLDER}/${RG}/${RG}.fixedcounts.gz | awk 'NF<3' | wc -l)
	        	echo "0 $noinsert_count" > ${OUT_FOLDER}/${RG}/${RG}.seqsummary.txt
			zcat ${OUT_FOLDER}/${RG}/${RG}.fixedcounts.gz | awk 'NF==3' | awk '{arr[$1]++;}END{for (i in arr){print length(i), arr[i]}}' | awk '{arr[$1]+=$2;}END{for(i in arr){print i, arr[i]}}' | sort -n >> ${OUT_FOLDER}/${RG}/${RG}.seqsummary.txt 
			echo "Generating 18-mers from 18-22mer fraction in library $RG ..."	
			zcat  ${OUT_FOLDER}/${RG}/${RG}.fixedcounts.gz | awk 'NF==3' | cut -f 1 | awk 'length($1)>=18 && length($1)<=22' |  awk '{print substr($1, length($1)-18+1)}' | awk 'BEGIN{sum=0;}{arr[$1]++;sum++;}END{for(i in arr){print i, arr[i], sum, arr[i]/sum}}' | sort -k 2,2nr >  ${OUT_FOLDER}/${RG}/${RG}.18mers_18-22.txt  
			
		fi
		echo "...done. Gzipping ${OUT_FOLDER}/${RG}/${RG}.18mers_18-22.txt for good measure, will delete this file later."
		cat ${OUT_FOLDER}/${RG}/${RG}.18mers_18-22.txt | gzip >  ${OUT_FOLDER}/${RG}/${RG}.18mers_18-22.txt.gz		
		echo "...gzipping done. Proceeding to run library stats for $RG"
	else
		echo "${OUT_FOLDER}/${RG}/${RG}.18mers_18-22.txt.gz exists, not regenerating. Proceeding to run library stats for ${RG} "
	fi

	total_reads=$(zcat $R1 | wc -l | awk '{print $1/4}')
	collapsed_reads=$(zcat ${OUT_FOLDER}/${RG}/${RG}.collapsed.gz | wc -l | awk '{print $1/4}') 
	reads_in_1822=$(zcat ${OUT_FOLDER}/${RG}/${RG}.18mers_18-22.txt.gz | head -n 1 | awk '{print $3}')
	zero_insert=$(cat ${OUT_FOLDER}/${RG}/${RG}.seqsummary.txt | awk '($1==0){print $2}')
	echo $RG $total_reads $collapsed_reads $reads_in_1822 $zero_insert | awk '{print $0, $3/$2, $4/$2, $5/$2}' > ${OUT_FOLDER}/${RG}/${RG}.librarystats.txt 
	echo "Library stats generated for ${RG} - processfastq complete." 
}
export -f processfastq

OUT_FOLDER="$(pwd -P)/guide_diversity_${NA}"
mkdir -p $OUT_FOLDER

##function that actually calls the processfastq method, running 12 jobs in parallel
stats() {
	PROMOTER="TAATACGACTCACTATAGAG"
	STEMLOOP="GTTTTAGAG"
	OUT_FOLDER=$3
	NA=$4
	processfastq $1 $2 $PROMOTER $STEMLOOP $OUT_FOLDER $NA
}
export -f stats
parallel --link -j12 stats ::: $(cat $FILES_PAIRED | cut -f 1 -d " ") ::: $(cat $FILES_PAIRED | cut -f 2 -d " ") ::: $OUT_FOLDER ::: $NA

find ${OUT_FOLDER} -name "*librarystats.txt" | sort | xargs -I xx cat xx > ${OUT_FOLDER}/LibraryStats.txt

## Pre-created database of guide sequence (18-mer), number of cutsites in reference genome, total cutsites (303 million-ish), and corresponding proportion of cutsites for said guide sequence
DATABASE="/mnt/bdbStorage01/data/reference/gRNAs/GRCh38.p10.guides.uniqN18counts.cutsitesummary.prop.txt.gz"

## Pre-created database of guide sequence (18-mer) present in the 90k array, number of cutsites in reference genome, and corresponding proportion of cutsites for said guide sequence
DB_90k="/mnt/bdbStorage01/data/reference/gRNAs/90k_genomeabundances_18mer_relprop.txt"

## Print out 18-mers in libraries that are legit guides present in genome, marked by number of cutsites (penultimate column) and corresponding proportion (final column) in the reference genome (GRCh38.fa). 
## Print out "0 0" as final two columns for 18-mers that don't have an exact hit in the database.
if [ ! -f ${OUT_FOLDER}/ProportionSummary_18-22.gz ]; then 
	awk ' function basename(file) {
		sub(".*/", "", file)
		return file
	  } 
		FNR==NR {
	        hash[$1]=$2" "$4;
	        next
	}{
	        if ($1 in hash)
	                {print basename(FILENAME), $1, $2, $3, $4, hash[$1];}
	        else {
	                print basename(FILENAME), $1, $2, $3, $4, "0", "0";}
	}' <(zcat $DATABASE) $(find $(pwd -P) -name "*.18mers_18-22.txt") | gzip >  ${OUT_FOLDER}/ProportionSummary_18-22.gz
	echo "${OUT_FOLDER}/ProportionSummary_18-22.gz generated."
else
	echo "${OUT_FOLDER}/ProportionSummary_18-22.gz exists, reusing for downstream calculations"
fi

if [ ! -f ${OUT_FOLDER}/GuideSummary_90k.gz ]; then
	find $(pwd -P) -name "*.18mers_18-22.txt" | xargs -I placeholder awk ' function basename(file) {
                sub(".*/", "", file)
                return file
          }
                FNR==NR {
                hash[$1]=$2" "$4;
                file_name=basename(FILENAME)
                next
        } {
                if ($1 in hash) {
                        print file_name, $1, $2, $3, $4, hash[$1];}
                else {
                        print file_name, $1, $2, $3, $4, "0", "0";
                }
        }' placeholder $DB_90k | gzip > ${OUT_FOLDER}/GuideSummary_90k.gz
else
	echo "${OUT_FOLDER}/GuideSummary_90k.gz exists, not regenerating" 
fi

#count how many 18mers in the 90k are missing:
zcat ${OUT_FOLDER}/GuideSummary_90k.gz | awk '$6==0' | awk '{print $1}' | awk '{arr[$1]++}END{for(i in arr){print i, arr[i]}}' >  ${OUT_FOLDER}/Missing_90k_18mers.txt

#count primer_specific oligos based on the design_file; put into respective array data file and plot
for f in $(cat $DESIGN | awk '{print $3}' | sort | uniq | grep -iv "diego"); do
	zcat ${OUT_FOLDER}/GuideSummary_90k.gz | \
	sed 's/.18mers_18-22.txt//' | \
	awk 'FNR==NR {hash[$1];next} $1 in hash' <(awk -v arr=$f '($3==arr){print $1}' $DESIGN) - | \
	awk 'FNR==NR{hash[$2]=$1;next} ($2 in hash) {print $0, hash[$2];}' ${NT_GD_SCRIPT_DIR}/gRNA_modules/${f}_primerpools.txt - | \
	awk '{arr[$1" "$8]+=$7}END{for (i in arr){print i, arr[i]}}' | gzip > ${OUT_FOLDER}/${f}_90kprimers.txt.gz
	Rscript ${NT_GD_SCRIPT_DIR}/gRNA_modules/plot_primer.R $OUT_FOLDER $f
done


#crap proportion measurement - (1 - column 8) + (column 10) in the generated file below. Column 8 = proportion non-18-22mers, column 10 = proportion of 18-22mers that are not legit guides

echo "Running noise measurement for libraries..."
zcat ${OUT_FOLDER}/ProportionSummary_18-22.gz | awk '$6==0' | awk '{arr[$1]+=$3}END{for(i in arr){print i, arr[i]}}' | sed 's/.18mers_18-22.txt//' | awk 'FNR==NR {hash[$1]=$2; next}($1 in hash){print $1, $2, $3, $4, $5, hash[$1], $6, $7, $8, hash[$1]/$2}' - ${OUT_FOLDER}/LibraryStats.txt > ${OUT_FOLDER}/LibraryStatsWithCrap.txt
echo "...done."

#unique guide and cutsite measurement
echo "Running unique guide and cutsite measurement..."
zcat ${OUT_FOLDER}/ProportionSummary_18-22.gz | awk '$6!=0' | awk '{guides[$1]++; cutsites[$1]+=$6}END{for(i in guides){print i, guides[i], cutsites[i]}}' | sed 's/.18mers_18-22.txt//' | sort > ${OUT_FOLDER}/UniqueGuidesCutsites.txt
echo "...done."

#Normalized counts for unique guides and cutsites : unique guides/cutsites per million reads sequenced.
#Note: this might not be correct as it assumes linear ascendency of unique guides/cutsites with greater sequencing - which doesn't quite hold. 
#I might remove this little one-liner later and just use pre-simulated data based on sampling from the 300 million GRCh38 guides
awk 'FNR==NR {hash[$1]=$2; next} ($1 in hash){print $1, $2, 1000000*$2/hash[$1], $3, 1000000*$3/hash[$1]}' ${OUT_FOLDER}/LibraryStats.txt ${OUT_FOLDER}/UniqueGuidesCutsites.txt > ${OUT_FOLDER}/NormalizedGuidesCutsites.txt

#cleanup - don't need the *18-22.txt files, we have the gzipped version
echo "Cleaning up big txt files, we already gzipped them..."
find ${OUT_FOLDER} -name "*_18-22.txt" | xargs -I xx rm -f xx
echo "...done."

##Plots
#Plot out guide length distribution, proportion useable reads, proportion 18-22mers, proportion zero inserts, proportion noise (might have called this CRAP in the code, please excuse)
echo "Generating library plots..."
Rscript $NT_GD_SCRIPT_DIR/gRNA_modules/plot_LibraryStats.R $OUT_FOLDER $DESIGN $NA
echo "...done."
echo "Generating guide plots..."
Rscript $NT_GD_SCRIPT_DIR/gRNA_modules/plot_GuideStats.R $OUT_FOLDER $DESIGN $NA
echo "...done. Pipeline complete."

