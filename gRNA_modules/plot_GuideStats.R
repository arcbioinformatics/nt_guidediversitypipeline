rm(list=ls())
#set mouse=i
args <- commandArgs(trailingOnly=TRUE)
if(length(args)!=3) {
	stop("Need working directory, design and nucleic acid type")
}
wd <- args[1]
design_file <- args[2]
na_type <- args[3]
ref_guides <- paste(Sys.getenv("NT_GD_SCRIPT_DIR"), "/gRNA_modules/Reference_Guides_", na_type, ".txt", sep="")
ref_libs <- paste(Sys.getenv("NT_GD_SCRIPT_DIR"), "/gRNA_modules/Reference_Lib_", na_type, ".txt", sep="")
library(data.table)
design <- fread(paste("cat ", design_file, " | awk '{print $1, $2}'", sep=""))
colnames(design) <- c("LIBRARY", "SOURCE")
setwd(wd)

#read and plot unique guides/cutsites
guide_file <- "UniqueGuidesCutsites.txt"
LibraryStats_file <- "LibraryStatsWithCrap.txt"

guide_stats <- read.table(guide_file, header=F, stringsAsFactors=F)
colnames(guide_stats) <- c("LIBRARY", "UNIQUE_GUIDES", "TOTAL_CUTSITES")

LibraryStats <- read.table(LibraryStats_file, header=F, stringsAsFactors=F)[,c(1,2)]
colnames(LibraryStats) <- c("LIBRARY", "TOTAL")

guide_stats_merged <- merge(design, guide_stats, by.x="LIBRARY", by.y="LIBRARY", sort) 
guide_stats_merged <- merge(guide_stats_merged, LibraryStats, by.x="LIBRARY", by.y="LIBRARY", sort)
guide_stats_merged$TYPE <- "TREATMENT"

#ref_Lib_file <- "/tars01/sradhakrishnan/git/nt_guidediversitypipeline/gRNA_modules/Reference_Guides_DNA.txt"
ref_Lib_file <- ref_guides
ref_stats <- read.table(ref_Lib_file, header=T, stringsAsFactors=F)
ref_stats$TYPE <- "REFERENCE"

#ref_GRCh38_file <- "/tars01/sradhakrishnan/git/nt_guidediversitypipeline/gRNA_modules/GRCh38.p10_hgg_sampled.txt"
ref_GRCh38_file <- paste(Sys.getenv("NT_GD_SCRIPT_DIR"), "gRNA_modules/GRCh38.p10_hgg_sampled.txt", sep="/")
ref_GRCh38 <- read.table(ref_GRCh38_file, header=T, stringsAsFactors=F)
ref_GRCh38$TYPE <- "REFERENCE_GENOME"

#LibraryStats_ref_file <- "/tars01/sradhakrishnan/git/nt_guidediversitypipeline/gRNA_modules/Reference_Lib_DNA.txt"
LibraryStats_ref_file <- ref_libs
LibraryStats_ref <- read.table(LibraryStats_ref_file, header=T, stringsAsFactors=F)
LibraryStats_ref$TYPE <- "REFERENCE"

datalib <- rbind(guide_stats_merged, ref_stats, ref_GRCh38)
datalib$LABEL_UNIQUEGUIDES <- ""
datalib$LABEL_TOTALCUTSITES <- ""
rows_needing_labels <- which(datalib$TYPE=="TREATMENT")
labels_uniqueguides <- paste(as.character(guide_stats_merged$LIBRARY), as.character(guide_stats_merged$UNIQUE_GUIDES), sep=":") 
labels_totalcutsites <- paste(as.character(guide_stats_merged$LIBRARY), as.character(guide_stats_merged$TOTAL_CUTSITES), sep=":") 
datalib$LABEL_UNIQUEGUIDES[rows_needing_labels] <- labels_uniqueguides
datalib$LABEL_TOTALCUTSITES[rows_needing_labels] <- labels_totalcutsites

library(ggrepel)
W <- 12
H <- 12

#xmax <- max(datalib[datalib$TYPE=="TREATMENT",]$TOTAL)+100000
xmax <- 10^(log10(max(datalib[datalib$TYPE=="TREATMENT",]$TOTAL))+0.2)
required <- datalib[datalib$TYPE=="REFERENCE_GENOME",]$TOTAL<=xmax
ymax_uniqueguides <- 10^(log10(max(datalib[datalib$TYPE=="REFERENCE_GENOME",]$UNIQUE_GUIDES[required]))+0.1)
p4 <- ggplot(datalib) +
	geom_point(aes(x=TOTAL, y=UNIQUE_GUIDES, colour=SOURCE, shape=TYPE), size=3) +
	geom_line(data=ref_GRCh38, aes(x=TOTAL, y=UNIQUE_GUIDES, group=SOURCE, colour=SOURCE)) +
	geom_abline(slope=1, intercept=0, linetype="dotdash") +
	geom_label_repel(aes(x=TOTAL, y=UNIQUE_GUIDES, label=LABEL_UNIQUEGUIDES), box.padding = 0.35, point.padding = 0.5, segment.color = 'grey50', size=3) +
	guides(shape=FALSE) +
                 theme(legend.title=element_text(size=18, face="bold"), legend.text=element_text(size=12, face="bold"), legend.position = "bottom",
              axis.title.x = element_text(face="bold", size=18),
              axis.title.y = element_text(face="bold", size=18),
              axis.text.x  = element_text(size=14, face="bold"),
              axis.text.y  = element_text(size=14, face="bold"),
              strip.text.x  = element_text(size=14, face="bold"),
              strip.text.y  = element_text(size=14, face="bold"),
              plot.margin = margin(0.5, 0.5, 0.5, 0.5, "cm")) +
    labs(x="SEQUENCING DEPTH (READ PAIRS)", y="UNIQUE GUIDES")  +
        expand_limits(y=0) +
	xlim(0, xmax) +
	ylim(0, ymax_uniqueguides)
ggsave("UNIQUEGUIDES_SEQUENCINGDEPTH.pdf", p4, width=W, height=H, units="in")


required <- datalib[datalib$TYPE=="REFERENCE_GENOME",]$TOTAL<=xmax
ymax_cutsites <- 10^(log10(max(datalib[datalib$TYPE=="REFERENCE_GENOME",]$TOTAL_CUTSITES[required]))+0.1)
p5 <- ggplot(datalib) +
	geom_point(aes(x=TOTAL, y=TOTAL_CUTSITES, colour=SOURCE, shape=TYPE), size=3) +
	geom_line(data=ref_GRCh38, aes(x=TOTAL, y=TOTAL_CUTSITES, group=SOURCE, colour=SOURCE)) + 
	geom_label_repel(aes(x=TOTAL, y=TOTAL_CUTSITES, label=LABEL_TOTALCUTSITES), box.padding = 0.35, point.padding = 0.5, segment.color = 'grey50', size=3) +
	guides(shape=FALSE) +
                 theme(legend.title=element_text(size=18, face="bold"), legend.text=element_text(size=12, face="bold"), legend.position = "bottom",
              axis.title.x = element_text(face="bold", size=18),
              axis.title.y = element_text(face="bold", size=18),
              axis.text.x  = element_text(size=14, face="bold"),
              axis.text.y  = element_text(size=14, face="bold"),
              strip.text.x  = element_text(size=14, face="bold"),
              strip.text.y  = element_text(size=14, face="bold"),
              plot.margin = margin(0.5, 0.5, 0.5, 0.5, "cm")) +
    labs(x="SEQUENCING DEPTH (READ PAIRS)", y="TOTAL CUTSITES")  +
        expand_limits(y=0) +
	xlim(0, xmax) +
	ylim(0, ymax_cutsites)
ggsave("TOTALCUTSITES_SEQUENCINGDEPTH.pdf", p5, width=W, height=H, units="in")


##missing guides in 90k
g90k_18mers_file <- "/mnt/bdbStorage01/data/reference/gRNAs/90k_genomeabundances_18mer_relprop.txt"
g90k_18mers_total <- nrow(read.table(g90k_18mers_file, header=F))
missing90kguides_file <- "Missing_90k_18mers.txt"
missing90kguides <- read.table(missing90kguides_file, header=F, stringsAsFactors=F)
colnames(missing90kguides) <- c("LIBRARY", "MISSING_90k_COUNT")
missing90kguides$LIBRARY <- gsub("_S.*","",missing90kguides$LIBRARY)
missing90kguides$PROPORTION_MISSING <- missing90kguides$MISSING_90k_COUNT/g90k_18mers_total

p6 <- ggplot(missing90kguides) +
	geom_point(aes(x="", y=PROPORTION_MISSING)) +
	geom_label_repel(aes(x="",y=PROPORTION_MISSING, label=LIBRARY), box.padding = 0.35, point.padding = 0.5, segment.color = 'grey50', size=3) +
	       theme(legend.title=element_text(size=18, face="bold"), legend.text=element_text(size=12, face="bold"), legend.position = "bottom",
              axis.title.x = element_text(face="bold", size=18),
              axis.title.y = element_text(face="bold", size=18),
              axis.text.x  = element_text(size=14, face="bold"),
              axis.text.y  = element_text(size=14, face="bold"),
              strip.text.x  = element_text(size=14, face="bold"),
              strip.text.y  = element_text(size=14, face="bold"),
              plot.margin = margin(0.5, 0.5, 0.5, 0.5, "cm")) +
    labs(x="", y="PROPORTION OF 90k GUIDES MISSING")  +
	 expand_limits(y=0)
ggsave("MISSINGPROPORTION_90kGUIDES.pdf", p6, width=W, height=H, units="in")
