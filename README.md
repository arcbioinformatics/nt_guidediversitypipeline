1) set env variable NT_GD_SCRIPT_DIR to your nt_guidediversitypipeline directory, wherever you've pulled it (add it to your .bashrc so you don't have to worry about it)

2) install R packages as needed:

install.packages("ggplot2")
install.packages("ggrepel")
install.packages("cowplot")

COL1: Sample name, include the _S number from the sequencer
COL2: Label for Sample
COL3: Guide batch; can be "170623" or "Diego" at this point. Talk to Srihari if you're sequencing a different batch.

Writing the design file:
COL1	COL2	COL3
SAMPLE_S1	LABEL_SAMPLE_S1	Diego
SAMPLE_S2	LABEL_SAMPLE_S2	170623
.	.	.
.	.	.
.	.	.
SAMPLE_Sn	LABEL_SAMPLE_Sn 170623
