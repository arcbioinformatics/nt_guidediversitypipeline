from pyfasta import Fasta
import re
import sys

f = Fasta(sys.argv[1])
#f = Fasta("/mnt/bdbStorage01/sradhakrishnan/tests/python/all_rRNA_sequences.txt")

for chr in f.keys():
    for match in re.finditer("G(?=G)", str(f[chr])):
	print(''.join([str(chr),'\t',str(match.end()-22),'\t',str(match.end()+1),'\t',f.sequence({'chr':str(chr), 'start':match.end()-21, 'stop':match.end()+1}),'\t',"0", '\t','+']))
    for match_neg in re.finditer("C(?=C)", str(f[chr])):
	print(''.join([str(chr),"\t",str(match_neg.start()),'\t',str(match_neg.start()+23),'\t',f.sequence({'chr':str(chr), 'start':match_neg.start()+1, 'stop':match_neg.start()+23}), '\t',"0",'\t','-']))
