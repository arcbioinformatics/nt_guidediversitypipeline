## INPUT: FASTA FILE
## OUTPUT: BASE FILE WITH GUIDE SEQUENCE, # CUTSITES, # TOTAL CUTSITES and CUTSITE PROPORTION

FASTA=$1

python ${NT_GD_SCRIPT_DIR}/basefiles/getn20_bed.py $FASTA | awk '$2>0'| gzip > ${FASTA}.NGG.bed.gz

zcat ${FASTA}.NGG.bed.gz |  
    bioawk -c bed '{if ($6=="-") { 
            print $0, substr(revcomp($4),3,18)
        } 
        else {
            print $0, substr($4, 3,18)
        }
    }' | awk '{arr[$7]++;}END{for(i in arr){print i, arr[i]}}' | gzip > ${FASTA}.N18.count.gz

awk 'FNR==NR {sum+=$2;next}{print $0, sum}' <(zcat ${FASTA}.N18.count.gz) <(zcat ${FASTA}.N18.count.gz) | awk '{print $0, $2/$3}' > ${FASTA}.guides.uniqN18counts.cutsitesummary.prop.txt.gz
rm -f ${FASTA}.N18.count.gz

