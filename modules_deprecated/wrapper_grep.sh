source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh

FILES=$1
TYPE=$2
ENZYME=`echo $3 | awk '{print tolower($1)}'`
#SAMPLE=$4

samplereads() {
    NO_LINES=4000000
    PRE="SAMPLE1MN"
    BASE1=`basename $1`
    BASE2=`basename $2`
    DIR=`dirname $1`
    zcat $1 | head -n $NO_LINES | gzip > ${DIR}/${PRE}_${BASE1} 
    zcat $2 | head -n $NO_LINES | gzip > ${DIR}/${PRE}_${BASE2} 
}

export -f samplereads
$GNU_PARALLEL_EXEC --record-env


SAMPLE=FALSE
if [ $SAMPLE == "TRUE" ]; then
#    $GNU_PARALLEL_EXEC --link -j0 samplereads ::: `awk '{print $1}' $FILES` ::: `awk '{print $2}' $FILES`
    OUTFILE="files_paired_corrected"
    
    #BASEDIR=`head -n 2 $FILES | cut -f 1 -d " " | xargs -I xx dirname xx`
    BASEDIR=`cat $FILES | head -n 2 | cut -f 1 -d " " | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/' | xargs -I xx dirname xx`
    find $BASEDIR -name "*.fastq.gz" | sort | paste -d " " - - | grep -v "UNPAIRED" > ${OUTFILE}
    FILES=${OUTFILE}
fi

if [ $TYPE == "DNA" ]; then
        FASTQ_SCRIPT="${MODULE_FOLDER}/${ENZYME}/processRawFastq_DNA_grep.sh"
else
        echo "processing RNA sequences only available for Cas9 at this point"
        FASTQ_SCRIPT="${MODULE_FOLDER}/cas9/processRawFastq_RNA_grep.sh"
fi

while IFS= read line
do
    IFS=' ' read -r array <<< "$line"
    f1="${array[0]}"
    f2="${array[1]}"
    bash $FASTQ_SCRIPT $f1 $f2
done < "$FILES"


