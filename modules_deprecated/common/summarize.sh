source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
#zcat /home/sradhakrishnan/st01/data/guide_diversity_pipeline/modules/guide_diversity/simulated_parsesamresults1.gz | awk '{printf "%s\t%s\t", $0, gsub("_","\t", $0); print $0;}' | cut -f 3,4,8 --complement  | awk 'BEGIN{OFS="\t";}{gsub("tcount", "", $4); gsub("ucount","",$5); gsub("len","",$3); print}' | sed '1s/^/SEQ\tGUIDE\tLEN\tTCOUNT\tUCOUNT\tGID\tCUTSITES\n/' > ucount_numbers

## This module summarizes the following features from the sequencing experiment:

## Total guides present
## Total unique guides present
## Total cutsites represented
## Relative abundance of guides
## Distribution curve (calling an R script)
## Crap estimation from sequencing experiment

#Rscript ${MODULE_FOLDER}/common/plot.R ${OUT_FOLDER}

summarize(){
    source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
    library=$1
    mm=$2
    if [ $3=="RNA"]; then
        zcat ${OUT_FOLDER}/${library}*mm${mm}*parsed.sam.gz | cut -f 1,3 | awk '{printf "%s\t%s\t", $0, gsub("_","\t", $0); print $0;}' | cut -f 3,4,8 --complement  | awk 'BEGIN{OFS="\t";}{gsub("tcount", "", $4); gsub("ucount","",$5); gsub("len","",$3); print}' | sed '1s/^/SEQ\tGUIDE\tLEN\tTCOUNT\tUCOUNT\tGID\tCUTSITES\n/' > ${OUT_FOLDER}/${library}.numbers.tsv
    else
        zcat ${OUT_FOLDER}/${library}*mm${mm}*parsed.sam.gz | cut -f 1,3 |  awk '{printf "%s\t%s\t", $0, gsub("_","\t", $0); print $0;}' | cut -f 3,4,7 --complement | awk 'BEGIN{OFS="\t";}{gsub("tcount", "", $4); gsub("len","",$3); print}' | sed '1s/^/SEQ\tGUIDE\tLEN\tTCOUNT\tGID\tCUTSITES\n/' > ${OUT_FOLDER}/${library}.numbers.tsv 
    fi
    


}

export -f summarize
$GNU_PARALLEL_EXEC --record-env

$GNU_PARALLEL_EXEC summarize :::`ls ${OUT_FOLDER}/*.sam.gz | sort | sed 's/_[0-9]\+.parsed.*//' | awk '{a[$1]++}END{for (i in a){print i}}'` ::: 0 1 ::: RNA 

Rscript summarize.R ${OUT_FOLDER} # /${library}.numbers.tsv
