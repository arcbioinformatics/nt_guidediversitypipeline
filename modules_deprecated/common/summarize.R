
library(data.table)
#f <- "~/st01/data/guide_diversity_pipeline/modules/ucount_numbers"
path <- commandArgs(trailingOnly=T)[1]
files <- list.files(path, pattern="*_numbers.tsv")
df <- do.call(rbind, lapply(files, function(f) {
    infile <- fread(f)
    unique_guides<-unique(infile$GUIDE)
    total_unique <- length(unique_guides)
    total_cutsites<- sum(sapply(unique_guides, function(x) {
        as.numeric(unlist(strsplit(x, "_"))[3])
    }))
    total_unique_library_sequences <- ifelse(is.null(infile$UCOUNT),sum(infile$TCOUNT), sum(infile$UCOUNT))
    #cat(paste(f, total_unique, total_cutsites,  total_unique_library_sequences, sep=" "))
    c(gsub("_numbers.tsv", "", f), total_unique, total_cutsites,  total_unique_library_sequences)
}))
colnames(df) <- c("LIBRARY", "TOTAL_UNIQUE", "TOTAL_CUTSITES", "TOTAL_LIBRARY_HITS")
write.table(df, "Numbers_df.tsv", quote=F, col.names=T, row.names=F, sep="\t")

