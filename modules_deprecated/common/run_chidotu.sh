## This module runs chidotu on each of the 4 fasta files against
## the respective fasta databases (assuming hashes and databases exist) and 
## produces a sam file.
source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh

LENGTH=$1
HASH=$2
INDEX=$3
INPUT_FASTQ=$4
DB_FASTA=$5

CHIDOTU_EXEC="${MODULE_FOLDER}/chidotu_files/${LENGTH}/chidotu"
export LD_LIBRARY_PATH="${MODULE_FOLDER}/chidotu_files/"
mkdir -p "${OUT_FOLDER}/sam"

RG=`basename ${INPUT_FASTQ} .fastq.gz`
$CHIDOTU_EXEC -c align -g $DB_FASTA -n 5 -p 60 -A 10 -B 10 -D 1 -E 10 -S 20 -T 6 -K 35220 -M 33000 -i $INDEX -h $HASH -f $INPUT_FASTQ -r $RG -o ${OUT_FOLDER}/sam/${RG} -a 


