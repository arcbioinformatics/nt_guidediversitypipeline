## This module parses chidotu output and quantitates guide representation
source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
INPUT_DIR=${OUT_FOLDER}/sam/${1}
parse(){
source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
    echo "writing to ${OUT_FOLDER}/parsesam/"
    #$1 is file
    #$2 is n20 length (for start position)
    #$3 is number of allowed mismatches (0/1 for now)
    #$4 is cutoff for seed (stored in $CUTOFF)
    base=`basename $1 .sam`
    CUTOFF=$4
    START=`expr 20 - $2 + 1`
   # echo $1 $START 
    
   # Filtering scheme:
    
    # look for proper sam lines with some kind of match (XM exists)
    # perl regex matches the GD:Z:_____ part; but looks only for the final M number followed by end of line. The ? makes it non-greedy, so the .* doesn't go and match digits that are part of the final M number. Regex prints line along with the the number of bases aligned
    # ensure start coordinate is correct
    # ensure number of mismatches tolerated is less than or equal to specified
    # ensure final match is at least as big as $CUTOFF

#    cat $1 | grep XM | perl -ne '@g = $_ =~ m/GD:Z:.*?(\d+)M$/; @h = $_ =~ m/XM:i:(\d)/; my $b = $_; chomp $b; if(@g & @h) { printf("%s\t%s\t%s\n", $b, @g, @h ) }' | awk -v start=$START '$4==start' | awk -v mm=$3 '$16<=mm' | awk -v cutoff=$CUTOFF '$15 >= cutoff' | gzip > ${base}.parsed.len${2}.mm${3}.parsed.sam.gz

    # Quantitating scheme:
    # Get cols 1,3 (seq_id and g_id; remember g_id has a guide index and total cutsites)
    # Do two seds to keep the _ in the seq_id but split the _ into tabs i g_id for simplicity
    # Sum up total lines per seq_id; this is total number of unique hits for a guide; Sum up total cutsites and print in same awk command
    mkdir -p ${OUT_FOLDER}/parsesam
    cat $1 | \
        grep XM | \
        perl -ne '@g = $_ =~ m/GD:Z:.*?(\d+)M$/; @h = $_ =~ m/XM:i:(\d)/; my $b = $_; chomp $b; if(@g & @h) { printf("%s\t%s\t%s\n", $b, @g, @h ) }' | \
        awk -v start=$START '$4==start' |\ 
        awk -v mm=$3 '$16<=mm' | \
        awk -v cutoff=$CUTOFF '$15 >= cutoff' |\
        gzip > ${OUT_FOLDER}/parsesam/${base}.parsed.len${2}.mm${3}.parsed.sam.gz
    zcat ${OUT_FOLDER}/parsesam/${base}.parsed.len${2}.mm${3}.parsed.sam.gz |\
        cut -f 1,3 |\
        sed 's/_/\t/g' |\ 
        sed 's/\t/_/' |\
        awk -v len=$2 -v mm=$3 'BEGIN{OFS="\t";}{totalguides[$1]++; totalcutsites[$1]+=$4}END{for (i in totalguides) {print i, len, mm, totalguides[i], totalcutsites[i]}}' |\ 
        gzip > ${OUT_FOLDER}/parsesam/${base}.parsed.len${2}.mm${3}.guidecutsitecounts.txt.gz

}

export -f parse
$GNU_PARALLEL_EXEC --record-env

$GNU_PARALLEL_EXEC --link -j0 parse ::: `ls $INPUT_DIR/part*sam` | sort` :::+ 17 18 19 20 ::: 0 1 ::: 10 

