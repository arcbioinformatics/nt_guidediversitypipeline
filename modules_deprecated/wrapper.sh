source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh

R1=$1
R2=$2
TYPE=$3

RG=`basename $R1 _R1_001.fastq.gz`

if [ $TYPE == "DNA" ]; then
    FASTQ_SCRIPT="${MODULE_FOLDER}/cas9/fastq_to_fasta_DNA.sh"
else
    FASTQ_SCRIPT="${MODULE_FOLDER}/cas9/fastq_to_fasta_RNA.sh"
fi

#bash $FASTQ_SCRIPT $R1 $R2 >> ${RG}.out 2 >> ${RG}.err

run_chidotu() {
    source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
#    bash ${MODULE_FOLDER}/common/run_chidotu.sh $1 $2 $3 $4 $5
    echo $1 $2 $3 $4 $5
}
export -f run_chidotu

$GNU_PARALLEL_EXEC --link -j0 run_chidotu ::: 17 18 19 20 :::+ `cat "${MODULE_FOLDER}/chidotu_files/hashes/cas9.hashfiles"` :::+ `cat "${MODULE_FOLDER}/chidotu_files/indexes/cas9.indexfiles"` :::+ `ls ${OUT_FOLDER}/${RG}/*.fastq.gz | sort` ::: ${MODULE_FOLDER}/chidotu_files/fastas/cas9.n20s.fa

