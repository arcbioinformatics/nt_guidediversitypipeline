## This module converts raw fastq files (RNA sequence input) into 6 output fasta
## files of sequence length 17, 18, 19 and 20, 21, 22

source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh

#R1="/tars01/sradhakrishnan/data/guide_diversity_pipeline/testdata/TRIMMED_PAIRED_R1_DHumA5_S2_R1_001.fastq.gz"
#R2="/tars01/sradhakrishnan/data/guide_diversity_pipeline/testdata/TRIMMED_PAIRED_R2_DHumA5_S2_R2_001.fastq.gz"

R1=$1
R2=$2
DB="/tars01/sradhakrishnan/data/guide_diversity_pipeline/modules/cas9/db"
DB_SORTED="/tars01/sradhakrishnan/data/guide_diversity_pipeline/modules/cas9/db_sorted"
#prefix for output files
RG=`basename $R1 _R1_001.fastq.gz`

#AdapterRemoval, keep only collapsed reads. These are high confidence reads we have evidence 
#by sequencing from both ends
AdapterRemoval --threads 20 --file1 $R1 --file2 $R2 --gzip --basename $RG --trimns --trimqualities --collapse
mkdir -p ${OUT_FOLDER}/${RG}
mv ${RG}.* ${OUT_FOLDER}/${RG}/
rm -f ${OUT_FOLDER}/${RG}/*.singleton.truncated.gz ${OUT_FOLDER}/${RG}/*.discarded.gz ${OUT_FOLDER}/${RG}/*.pair1.truncated.gz ${OUT_FOLDER}/${RG}/*.pair2.truncated.gz ${OUT_FOLDER}/${RG}/*.collapsed.truncated.gz

#Tease RG the middle sequence from the collapsed fastq; include how many times a given UMI combination is present
#Ultimately, sum of first column = total number of guides sequenced BEFORE accounting for PCR duplicates;
#            total number of lines = total number of guides sequenced AFTER accounting for PCR duplicates

#Command below:
# First spread RG the fastq into a single line
# Get the read
# Use the regex to filter RG the middle sequence. Looking for upto 6Gs, being non-greedy to match Gs in positions 4-6, followed by N17-20 followed by the stem loop (GTTTT.*GGCTA) followed by a 5-8 base UMI, and print the UMIs with the middle sequence
# Sort output and uniq -c to get number of times a sequence shows up with a given UMI combination
# Next collate the total number of times a sequence appears before and after PCR using the awk command, gzip and print the number RG.

zcat ${OUT_FOLDER}/${RG}/${RG}.collapsed.gz | paste - - - - | cut -f2 |  perl -ne 'm/GGG.{0,3}?(\w{0,140})GTTTT.*GGCTA(\w{5,8})/ && print "$1\t$2\n"' | sort -T $TMP_FOLDER | uniq -c | awk '{beforePCR[$2]=beforePCR[$2]+$1; afterPCR[$2]++;}END{for(i in beforePCR){print i, beforePCR[i], afterPCR[i]}}' | gzip > ${OUT_FOLDER}/${RG}/${RG}.all.prepostPCR.counts.gz
zcat ${OUT_FOLDER}/${RG}/${RG}.all.prepostPCR.counts.gz | awk '(NF!=3){print "0", $2}' >> ${OUT_FOLDER}/${RG}/${RG}.summary
zcat ${OUT_FOLDER}/${RG}/${RG}.all.prepostPCR.counts.gz | awk '(NF==3){print length($1), $3}' |  awk '{arr[$1]=arr[$1]+$2}END{for (i in arr) {print i, arr[i]}}' | sort -n >> ${OUT_FOLDER}/${RG}/${RG}.summary

getsubset() {
source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
    #  ${RG}.prepostPCR.counts is $1
    # $2 is 17, 18, 19 or 20
    base=`basename $1 .all.prepostPCR.counts.gz`
    lines=`zcat $1 | wc -l`
    digits="%0`echo "$lines" | grep -oE [[:digit:]] | wc -l`g"
    sep=`expr $2 + 1`
    qual=`seq -s H $sep | sed 's/[0-9]*//g'`
    zcat  $1 | awk -v var="$2" 'length($1)==var' | awk -v qual="$qual" -v len=$2 'BEGIN{OFS="\n";}{print ">seq_index"NR"_len"len"_tcount"$3, $1}' | gzip > ${OUT_FOLDER}/${base}/${base}_SSW_${2}.fasta.gz
}

export -f getsubset
$GNU_PARALLEL_EXEC --record-env
$GNU_PARALLEL_EXEC -j 0 getsubset ::: ${OUT_FOLDER}/${RG}/${RG}.all.prepostPCR.counts.gz ::: 17 18 19 20 21 22 


ls ${OUT_FOLDER}/${RG}/${RG}*.fasta.gz | sort > ${RG}_tmpFiles 


bash ${MODULE_FOLDER}/cas9/getNumberOfLegitGuides_grep.sh ${RG}_tmpFiles $DB $DB_SORTED



Rcommand="Rscript ${MODULE_FOLDER}/cas9/plotSeqLen_withREF_RNA.R ${OUT_FOLDER}/${RG} $REFERENCE_DATA_FOLDER"
echo $Rcommand
eval $Rcommand


