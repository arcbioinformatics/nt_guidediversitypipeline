source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
makehashindex() {
    prog_dir="/tars01/sradhakrishnan/data/guide_diversity_pipeline/modules/chidotu_files"
    executable=""
    mask=""
    case $2 in
        17)
            executable="chidotu17"
            mask="11101110110111"
            ;;
        18)
            executable="chidotu18"
            mask="111011101110111"
            ;;
        19) 
            executable="chidotu19"
            mask="11011011011011011"
            ;;
        20)
            executable="chidotu20"
            mask="11011101110111011"
            ;;
    esac
    #echo ${prog_dir}/${executable}   
    zcat $1 | ${prog_dir}/${executable} -c index -g - -m $mask -i cpf1.index${2}
    zcat $1 | ${prog_dir}/${executable} -c hash -i cpf1.index${2} -j 8 -h cpf1.hash${2}

}
export -f makehashindex
$GNU_PARALLEL_EXEC --record-env

$GNU_PARALLEL_EXEC --link -j0 makehashindex ::: `ls *uniqN1*fa.gz` ::: 17 18 19
