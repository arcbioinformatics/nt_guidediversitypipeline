## This module converts raw fastq files (DNA sequence input) into 4 output fastq
## files of sequence length 17, 18, 19 and 20. 

source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh


R1=$1
R2=$2
PROMOTER_SEQ=$3
#T7="TAATACGACTCACTATAGAG"
#prefix for output files
RG=`basename $R1 _R1_001.fastq.gz`

#AdapterRemoval, keep only collapsed reads. These are high confidence reads we have evidence 
#by sequencing from both ends
AdapterRemoval --threads 20 --file1 $R1 --file2 $R2 --gzip --basename $RG --trimns --trimqualities --collapse
mkdir -p ${OUT_FOLDER}/${RG}
mv ${RG}.* ${OUT_FOLDER}/${RG}/
rm -f ${OUT_FOLDER}/${RG}/*.singleton.truncated.gz ${OUT_FOLDER}/${RG}/*.discarded.gz ${OUT_FOLDER}/${RG}/*.pair1.truncated.gz ${OUT_FOLDER}/${RG}/*.pair2.truncated.gz ${OUT_FOLDER}/${RG}/*.collapsed.truncated.gz

#Tease out the middle sequence from the collapsed fastq
zcat ${OUT_FOLDER}/${RG}/${RG}.collapsed.gz | grep -o "${PROMOTER_SEQ}.*GTTTT" | sed -e "s/${PROMOTER_SEQ}//" -e 's/GTTTT$//' | awk '{arr[$1]++;}END{for (i in arr){print i, arr[i]}}' | gzip > ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz

#Function to get sequences n ($2) bases long from tmpcounts.gz file generated above ($1) and store them
#in a fastq file. This function will be run in $GNU_PARALLEL_EXEC across all lengths
getsubset() {
    source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
    base=`basename $1 .tmpcounts.gz`
    sep=`expr $2 + 1`
    qual=`seq -s H $sep | sed 's/[0-9]*//g'`
    #echo $qual
    mkdir -p  ${OUT_FOLDER}/${base}
    zcat  $1 | awk -v var="$2" 'length($1)==var' | awk -v qual="$qual" -v len=$2 'BEGIN{OFS="\n";}{print "@seq_index"NR"_len"len"_tcount"$2,$1,"+", qual}' | gzip > ${OUT_FOLDER}/${base}/${base}_${2}.fastq.gz
}

export -f getsubset
$GNU_PARALLEL_EXEC --record-env
$GNU_PARALLEL_EXEC -j4 getsubset ::: ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz ::: 17 18 19 20 


