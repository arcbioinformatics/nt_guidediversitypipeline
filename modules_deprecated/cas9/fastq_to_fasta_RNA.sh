## This module converts raw fastq files (RNA sequence input; SSW libraries) into 4 output fastq
## files of sequence length 17, 18, 19 and 20. This module also adjusts for PCR duplications using UMI information
## in the sequence.

source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh

## SEQUENCEID_INDEX_TOTALCOUNT_UNIQUECOUNT

#R1="/tars01/sradhakrishnan/data/guide_diversity_pipeline/testdata/TRIMMED9394_PAIRED_R1_DHumA1_SSW_S1_R1_001.fastq.gz"
#R2="/tars01/sradhakrishnan/data/guide_diversity_pipeline/testdata/TRIMMED9394_PAIRED_R2_DHumA1_SSW_S1_R2_001.fastq.gz"

#prefix for output files

R1=$1
R2=$2

RG=`basename $R1 _R1_001.fastq.gz`

#AdapterRemoval, keep only collapsed reads. These are high confidence reads we have evidence
#by sequencing from both ends
AdapterRemoval --threads 20 --file1 $R1 --file2 $R2 --gzip --basename $RG --trimns --trimqualities --collapse
mkdir -p ${OUT_FOLDER}/${RG}
mv  ${RG}.* ${OUT_FOLDER}/${RG}/
rm -f ${OUT_FOLDER}/${RG}/*.singleton.truncated.gz ${OUT_FOLDER}/${RG}/*.discarded.gz ${OUT_FOLDER}/${RG}/*.pair1.truncated.gz ${OUT_FOLDER}/${RG}/*.pair2.truncated.gz ${OUT_FOLDER}/${RG}/*.collapsed.truncated.gz


#Tease RG the middle sequence from the collapsed fastq; include how many times a given UMI combination is present
#Ultimately, sum of first column = total number of guides sequenced BEFORE accounting for PCR duplicates; 
#            total number of lines = total number of guides sequenced AFTER accounting for PCR duplicates

#Command below:
# First spread RG the fastq into a single line 
# Get the read
# Use the regex to filter RG the middle sequence. Looking for upto 6Gs, being non-greedy to match Gs in positions 4-6, followed by N17-20 followed by the stem loop (GTTTT.*GGCTA) followed by a 5-8 base UMI, and print the UMIs with the middle sequence
# Sort output and uniq -c to get number of times a sequence shows up with a given UMI combination
# Next collate the total number of times a sequence appears before and after PCR using the awk command, gzip and print the number RG.

zcat ${OUT_FOLDER}/${RG}/${RG}.collapsed.gz | paste - - - - | cut -f2 |  perl -ne 'm/GGG(.{0,3})?(\w{17,20}?)GTTTT.*GGCTA(\w{5,8})/ && print "$1\t$2\t$3\n"' | sort -T $TMP_FOLDER | uniq -c | awk '{beforePCR[$3]=beforePCR[$3]+$1; afterPCR[$3]++;}END{for(i in beforePCR){print i, beforePCR[i], afterPCR[i]}}' | gzip > ${OUT_FOLDER}/${RG}/${RG}.prepostPCR.counts.gz 

getsubset() {
source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
    #  ${RG}.prepostPCR.counts is $1
    # $2 is 17, 18, 19 or 20
    
    
    base=`basename $1 .prepostPCR.counts.gz`
    lines=`zcat $1 | wc -l`
    digits="%0`echo "$lines" | grep -oE [[:digit:]] | wc -l`g"
    sep=`expr $2 + 1`
    qual=`seq -s H $sep | sed 's/[0-9]*//g'`
    zcat  $1 | awk -v var="$2" 'length($1)==var' | awk -v qual="$qual" -v len=$2 'BEGIN{OFS="\n";}{print "@seq_index"NR"_len"len"_tcount"$2"_ucount"$3, $1, "+", qual}' | gzip > ${OUT_FOLDER}/${base}/${base}_SSW_${2}.fastq.gz
}

export -f getsubset
$GNU_PARALLEL_EXEC --record-env
$GNU_PARALLEL_EXEC -j 0 getsubset ::: ${OUT_FOLDER}/${RG}/${RG}.prepostPCR.counts.gz ::: 17 18 19 20
