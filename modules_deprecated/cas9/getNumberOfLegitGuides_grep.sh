# This script deals with two files in parallel: fasta file with N20 sequences and corresponding length db with guide sequences; 
# does grep (perfect matches) and returns # of legit guides and their abundance

source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh

grep_func() {
    source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
    query=$1
    query_pre=${query/.fasta.gz/}
    dir=`dirname $query`
    subject=$2
    subject_sorted=$3
    if [[ $query =~ 21.fasta.gz ]]; then
        zcat $query | paste -d " " - - | sed 's/>//' | awk '{print $1, substr($2,2)}' | awk '{arr[$2]=$1"|"arr[$2]}END{for (i in arr) {print ">"arr[i]"\n",i}}' | sed 's/^ *//' | gzip > ${query_pre}.20.fasta.gz
        query="${query_pre}.20.fasta.gz"
    fi

    if [[ $query =~ 22.fasta.gz ]]; then
        zcat $query | paste -d " " - - | sed 's/>//' | awk '{print $1, substr($2,3)}' | awk '{arr[$2]=$1"|"arr[$2]}END{for (i in arr) {print ">"arr[i]"\n",i}}' | sed 's/^ *//' | gzip > ${query_pre}.20.fasta.gz
        query="${query_pre}.20.fasta.gz"
    fi
    echo "Querying $query in $subject"
    bq=`basename $query`
    bs=`basename $subject`
  
    zgrep -F -B 1 -wf <(zgrep -v "^>" $query) $subject | gzip > ${dir}/${bq}.${bs}.res.gz
    zcat $query | paste -d " " - - | sed 's/>//' | sort -T $TMP_FOLDER -k 2,2 | join -a1 -e "0" -o 0 1.1 2.1 -1 2 -2 2 - $subject_sorted | gzip >  ${dir}/${bq}.${bs}.join.res.gz
}

export -f grep_func

$GNU_PARALLEL_EXEC --record-env
echo "Running grep/join in parallel"
#input 1: query; input 2: db fasta; input 3: tsv db sorted by sequence
$GNU_PARALLEL_EXEC --link -j0 grep_func ::: `cat $1` ::: `cat $2` ::: `cat $3`

PRE_LIB=`head -n 1 $1 | sed 's/\(.*_S[0-9]\+\)_.*/\1/'`


summarize_func() {
    source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
    file=$1
    if [ -f ${file}.guidestats ]; then
        rm -f ${file}.guidestats
    fi
    echo "Summarizing grep/join results for ${file}"
    cat ${file}*.res.gz | zgrep "^>" | sed 's/>//' | sed 's/|/\n/g' | awk NF | sort -T $TMP_FOLDER | uniq > ${file}.grep.temp
    echo -ne "Unique_guides\t" >> ${file}.guidestats
    cat ${file}.grep.temp | wc -l >> ${file}.guidestats
    echo -ne "Cutsites\t" >> ${file}.guidestats
    awk 'BEGIN{FS="_"}{sum+=$3}END{print sum}' ${file}.grep.temp >> ${file}.guidestats

    zcat ${file}*join.res.gz | awk '($3!=0){print $3}' | sed 's/|/\n/g' | awk NF | sort -T $TMP_FOLDER | uniq > ${file}.join.temp
#    rm ${file}.temp

    if [ -f ${file}.guidestats.90k ]; then
        rm -f ${file}.guidestats.90k
    fi
    file_90k="/tars01/sradhakrishnan/ref/gRNAs/GRCh38.79k.GIDs"
    echo -ne "Total_90k_guides\t" >> ${file}.guidestats.90k
    zgrep -F -f $file_90k  ${file}*join.res.gz | cut -f 2 -d " " | cut -f 4 -d "_" | sed 's/tcount//' | awk '{sum+=$1}END{print sum}' >> ${file}.guidestats.90k
    echo -ne "Total_guides\t" >> ${file}.guidestats.90k
    zcat  ${file}*join.res.gz | cut -f 2 -d " " | cut -f 4 -d "_" | sed 's/tcount//' | awk '{sum+=$1}END{print sum}' >> ${file}.guidestats.90k

}

export -f summarize_func
$GNU_PARALLEL_EXEC --record-env
$GNU_PARALLEL_EXEC -j0 summarize_func ::: $PRE_LIB
