

doJoin() {
    b=`basename $1 .fa`
    split -l10000000 -a 2 $1 "tmp_${b}"
    parallel_sort() {
      cat $1 | paste -d " " - - | sed 's/>//' | sort -T ~/st01/tmp_sort -k 2,2 -o ${1}.subsorted
    }
    export -f parallel_sort
    parallel --record-env
    parallel -j50 parallel_sort ::: `ls -1 tmp_${b}*`

    sort -m tmp_${b}*subsorted -k 2,2 -o ${b}.sorted.fa
    #paste -d " " - - | sed 's/>//' | sort -T ~/st01/tmp_sort -k 2,2 | gzip > ${b}.sorted.fa.gz
}
export -f doJoin
parallel --record-env
parallel -j3 doJoin ::: `cat db | sort | uniq | tail -n +2`

#cat /home/sradhakrishnan/data/reference/GRCh38.p10.guides.uniqN17counts.fa | head -n 100 | paste -d " " - - | sed 's/>//' | sort -k 2,2 | less
