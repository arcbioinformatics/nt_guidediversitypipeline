args <- commandArgs(trailingOnly=T)
wd <- args[1]
#refdir <- args[2]
#wd <- "~/st01/data/SequencingRuns/runs93_94_95/run95/Analysis/diversity/guide_diversity/TRIMMED_PAIRED_R1_DHumA1_S1"
setwd(wd)
files_treatment <- list.files(wd, pattern="*.summary", recursive=T, full.names=T)
guide_stats <- list.files(wd, pattern="*.guidestats", recursive=T, full.names=T)

guide_stats_df <- read.table(guide_stats, header=F)

library(ggplot2)
library(cowplot)
library(data.table)

getDF <- function(file, label) {
    lengths <- read.delim(file, sep=" ", header=F) 
    colnames(lengths) <- c("LENGTH", "COUNT")
    lengths$LIBRARY <- label
    #lengths$TYPE <- ifelse(grepl("REF_TCHum", label), "REFERENCE", label)
    attach(lengths)
    lengths_ordered <- lengths[order(LENGTH),]
    detach(lengths)
    lengths_ordered$CUM_COUNT <- cumsum(lengths_ordered$COUNT)
    lengths_ordered$CUMUL_COUNT_FRACTION <- lengths_ordered$CUM_COUNT/sum(lengths_ordered$COUNT)
    lengths_ordered
}                

lib <- gsub("TRIMMED_PAIRED_R1_", "", basename(gsub(".summary", "", files_treatment)))
#reflib <-  gsub(".summary", "", basename(files_reference))
treatment_df <- do.call(rbind, mapply(getDF, files_treatment, lib, SIMPLIFY=FALSE))
#reference_df <- do.call(rbind, mapply(getDF, files_reference, reflib, SIMPLIFY=FALSE))
#rownames(reference_df) <- NULL
rownames(treatment_df) <- NULL
all <- treatment_df
#all <- rbind(reference_df, treatment_df)

#guide statistics numbers
getFrac <- function(l, df, length) {
    round(df[df$LENGTH==length & df$LIBRARY==l,]$CUMUL_COUNT_FRACTION - df[df$LENGTH==length-1 & df$LIBRARY==l,]$CUMUL_COUNT_FRACTION, 3)
}

y_20 <- getFrac(lib, treatment_df, 20) 
y_21 <- getFrac(lib, treatment_df, 21)

#l18_19_ref <- data.frame(L18_19=sapply(reflib, getFrac, df=reference_df, length=18) + sapply(reflib, getFrac, df=reference_df, length=18), TYPE="REFERENCE")
l18_19 <- data.frame(L18_19=y_20+y_21, LIBRARY=lib)
agg <- tapply(l18_19$L18_19, l18_19$LIBRARY, mean)

gs <- paste(guide_stats_df$V1[1], ": ", guide_stats_df$V2[1], "\n", guide_stats_df$V1[2], ": ", guide_stats_df$V2[2], sep="")


#adapterremoval_numbers
getARnumbers <- function(dir) {
    library(data.table)
    ar_settings_ <- list.files(dir, pattern="*.settings", recursive=T, full.names=T)
    do.call(rbind, lapply(ar_settings_, function(x) {
    ar_ <- fread(paste("cat ", x, " | grep 'Total number of read pairs:\\|Number of full-length collapsed pairs' | awk '{print $6}' | paste -d ' ' - -", sep=""))
    data.frame(TOTAL=ar_$V1, MERGED=ar_$V2, FRAC_LOST=round(1-(ar_$V2/ar_$V1), 3))
    }))
}
#getARnumbers("/tars01/sradhakrishnan/data/SequencingRuns/run57_TC/diversity_pipeline/guide_diversity/referenceData")

#ar_df_ref <- getARnumbers(refdir)
#ar_df_ref$TYPE="REFERENCE"
ar_df_lib <- getARnumbers(wd)
ar_df_lib$LIBRARY=lib
#ar_all <- rbind(ar_df_ref, ar_df_lib)
ar_all <- ar_df_lib

library(RColorBrewer)
myColors=c("#CC0000","#0000CC") 
names(myColors) = sort(unique(all$LIBRARY)) #levels(all$TYPE)
myLineType=c("longdash", "solid")
names(myLineType) <- sort(unique(all$LIBRARY))
p1 <- ggplot(treatment_df, aes(x=LENGTH, y=COUNT)) + 
      geom_line(colour=myColors[which(names(myColors)!="REFERENCE")]) 
       

      p2 <- ggplot(all,  aes(x=LENGTH, y=CUMUL_COUNT_FRACTION, group=LIBRARY)) + 
        geom_line(aes(linetype=LIBRARY, colour=LIBRARY), size=1.2) + 
        scale_colour_brewer(palette="Paired") +
        scale_colour_discrete(guide=FALSE) + 
        scale_colour_manual(values=myColors) +
        scale_linetype_manual(values=myLineType) +
        annotate("text", x=50, y=0.25, label=paste("Read pairs used: ", ar_df_lib$MERGED[1], " / ", ar_df_lib$TOTAL[1],"\n (fraction lost: ", ar_df_lib$FRAC_LOST[1], ")\n\n", names(agg)[1],  " lengths 17-22: \n ", getFrac(lib, treatment_df, 17),", ", getFrac(lib, treatment_df, 18), ", ", getFrac(lib, treatment_df, 19),", ", getFrac(lib, treatment_df, 20),", ", getFrac(lib, treatment_df, 21),", ", getFrac(lib, treatment_df, 22),", ","\n\n", gs, sep="")) +
        theme(legend.position="top")

#p3 <- ggplot(l18_19) + geom_boxplot(aes(x="", y=L18_19)) + 
#    geom_jitter(aes(x="",y=L18_19, colour=TYPE), size=2,  show.legend=T) +
#    scale_colour_manual(values=myColors) + 
#    labs(x="", y="Fraction of 18- and 19-mers", sep="")

#p4 <- ggplot(ar_all) + geom_boxplot(aes(x="", y=FRAC_LOST)) +
#    geom_jitter(aes(x="", y=FRAC_LOST, colour=TYPE), size=2, show.legend=T) +
#    scale_colour_manual(values=myColors) +
#    labs(x="", y="AdapterRemoval fraction lost")

cowplot <- plot_grid(p1, p2,  labels=c("A", "B"), align="h", ncol=2)
ggsave(paste(lib,"_SeqDistributionPlots.pdf", sep=""), cowplot, width=14, height=7, units="in")


