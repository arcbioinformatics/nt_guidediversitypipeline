## This module converts raw fastq files (DNA sequence input) into 4 output fastq
## files of sequence length 17, 18, 19 and 20. 

source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh

#R1="/tars01/sradhakrishnan/data/guide_diversity_pipeline/testdata/TRIMMED_PAIRED_R1_DHumA5_S2_R1_001.fastq.gz"
#R2="/tars01/sradhakrishnan/data/guide_diversity_pipeline/testdata/TRIMMED_PAIRED_R2_DHumA5_S2_R2_001.fastq.gz"

R1=$1
R2=$2
DB="/tars01/sradhakrishnan/data/guide_diversity_pipeline/modules/cpf1/db_human"
DB_SORTED="/tars01/sradhakrishnan/data/guide_diversity_pipeline/modules/cpf1/db_human_sorted"
#prefix for output files
RG=`basename $R1 _R1_001.fastq.gz`

#AdapterRemoval, keep only collapsed reads. These are high confidence reads we have evidence 
#by sequencing from both ends
AdapterRemoval --threads 20 --file1 $R1 --file2 $R2 --gzip --basename $RG --trimns --trimqualities --collapse
mkdir -p ${OUT_FOLDER}/${RG}
mv ${RG}.* ${OUT_FOLDER}/${RG}/
rm -f ${OUT_FOLDER}/${RG}/*.singleton.truncated.gz ${OUT_FOLDER}/${RG}/*.discarded.gz ${OUT_FOLDER}/${RG}/*.pair1.truncated.gz ${OUT_FOLDER}/${RG}/*.pair2.truncated.gz ${OUT_FOLDER}/${RG}/*.collapsed.truncated.gz

#Tease out the middle sequence from the collapsed fastq
zcat ${OUT_FOLDER}/${RG}/${RG}.collapsed.gz | grep -o "ACTGTTGTAGAT.*CGAGACCGAC" | sed -e 's/ACTGTTGTAGAT//' -e 's/CGAGACCGAC$//' | sort -T $TMP_FOLDER | uniq -c | awk '{print $2, $1}' | gzip > ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz
zcat ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz | awk '(NF==2){print length($1), $2}' |  awk '{arr[$1]=arr[$1]+$2}END{for (i in arr) {print i, arr[i]}}'  > ${OUT_FOLDER}/${RG}/${RG}.summary
zcat ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz | awk '(NF==1){print "0", $1}' >> ${OUT_FOLDER}/${RG}/${RG}.summary 

#Function to get sequences n ($2) bases long from tmpcounts.gz file generated above ($1) and store them
#in a fastq file. This function will be run in $GNU_PARALLEL_EXEC across all lengths
getsubset() {
    source /tars01/sradhakrishnan/git/nt_guidediversitypipeline/modules/env.sh
    base=`basename $1 .tmpcounts.gz`
    sep=`expr $2 + 1`
#    qual=`seq -s H $sep | sed 's/[0-9]*//g'`
    #echo $qual
    mkdir -p  ${OUT_FOLDER}/${base}
    zcat  $1 | awk -v var="$2" 'length($1)==var' | awk -v qual="$qual" -v len=$2 'BEGIN{OFS="\n";}{print ">seq_index"NR"_len"len"_tcount"$2,$1}' | gzip > ${OUT_FOLDER}/${base}/${base}_${2}.fasta.gz
}

export -f getsubset
$GNU_PARALLEL_EXEC --record-env

$GNU_PARALLEL_EXEC -j 0 getsubset ::: ${OUT_FOLDER}/${RG}/${RG}.tmpcounts.gz ::: 17 18 19 20 21 22 

ls ${OUT_FOLDER}/${RG}/${RG}*.fasta.gz | sort > ${RG}_tmpFiles 

bash ${MODULE_FOLDER}/cpf1/getNumberOfLegitGuides_grep.sh ${RG}_tmpFiles $DB $DB_SORTED
#rm -f ${RG}_tmpFiles



Rcommand="Rscript ${MODULE_FOLDER}/cpf1/plotSeqLen.R ${OUT_FOLDER}/${RG}" #$REFERENCE_DATA_FOLDER"
echo $Rcommand
eval $Rcommand


