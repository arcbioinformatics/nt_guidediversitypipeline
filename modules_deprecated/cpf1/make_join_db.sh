

doJoin() {
    b=`basename $1 .fa`
    zcat $1 | split -l10000000 -a 2 - "tmp_${b}"
    parallel_sort() {
      cat $1 | paste -d " " - - | sed 's/>//' | sort -T ~/st01/tmp_sort -k 2,2 -o ${1}.subsorted
    }
    export -f parallel_sort
    parallel --record-env
    parallel -j50 parallel_sort ::: `ls -1 tmp_${b}*`

    sort -T ~/st01/tmp_sort -m tmp_${b}*subsorted -k 2,2 -o ${b}.sorted.fa
    cat ${b}.sorted.fa | gzip > ${b}.sorted.fa.gz
    #paste -d " " - - | sed 's/>//' | sort -T ~/st01/tmp_sort -k 2,2 | gzip > ${b}.sorted.fa.gz
}
export -f doJoin
parallel --record-env
parallel -j4 doJoin ::: `cat db_human | sort | uniq`

#cat /home/sradhakrishnan/data/reference/GRCh38.p10.guides.uniqN17counts.fa | head -n 100 | paste -d " " - - | sed 's/>//' | sort -k 2,2 | less
